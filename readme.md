# Modable

UI Library for Vue 3

### Base on
- Vue 3
- TailwindCss
- Typescript

### How to use

Install:
```shell
npm i modable
```
Using in app entry point:
```ts
import {createApp} from 'vue'
import App from './App.vue'
import Modable from "modable"
import '../node_modules/modable/dist/modable.css'
// Choose one of the following themes
import '../node_modules/modable/dist/light.css'
import '../node_modules/modable/dist/dark.css'


createApp(App).use(Modable).mount('#app')
```

Copy fonts to public assets:

```shell
cp -r node_modules/modable/dist/fonts public
```
Using in vue components:

```vue
<template>
    <ComButton>Click me</ComButton>
</template>
```
