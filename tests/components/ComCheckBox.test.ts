import { mount } from '@vue/test-utils';
import { describe, it, expect } from 'vitest';
import { SelectMultipleProps, ComCheckBox } from '../../src';

const propsData: SelectMultipleProps = {
    options: [
        { value: '1', name: 'Option 1' },
        { value: '2', name: 'Option 2' },
        { value: '3', name: 'Option 3' }
    ],
    modelValue: [
        { value: '1', name: 'Option 1' }
    ]
};

describe('ComCheckBox', () => {
    let wrapper;

    beforeEach(() => {
        wrapper = mount(ComCheckBox, {
            props: propsData,
        });
    });

    it('renders the correct classes and options', () => {
        const div = wrapper.find('div.comCheckBox');
        expect(div.exists()).toBe(true);

        const labels = div.findAll('label');
        expect(labels).toHaveLength(propsData.options.length);

        propsData.options.forEach((option, index) => {
            const label = labels[index];
            expect(label.find('input').attributes('value')).toBe(option.value);
            expect(label.find('span.comCheckBox__label').text()).toBe(option.name);
        });
    });

    it('emits update:modelValue when checkbox is changed', async () => {
        const checkboxes = wrapper.findAll('input[type="checkbox"]');

        // Kiểm tra và thay đổi trạng thái của các checkbox
        await checkboxes[1].setChecked(true);
        await checkboxes[2].setChecked(true);

        // Kiểm tra sự kiện emit
        const emittedEvents = wrapper.emitted('update:modelValue');
        expect(emittedEvents).toBeTruthy();
        expect(emittedEvents.length).toBe(2);

        // Kiểm tra giá trị emitted
        expect(emittedEvents[0][0]).toEqual([
            { value: '1', name: 'Option 1', is_selected: true },
            { value: '2', name: 'Option 2', is_selected: true}
        ]);

        expect(emittedEvents[1][0]).toEqual([
            { value: '1', name: 'Option 1', is_selected: true},
            { value: '2', name: 'Option 2', is_selected: true },
            { value: '3', name: 'Option 3', is_selected: true }
        ]);
    });
});
