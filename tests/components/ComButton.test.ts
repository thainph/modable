import {mount} from '@vue/test-utils';
import {describe, expect, it, vi} from 'vitest';
import {ButtonSize, ButtonStyle, ButtonProps, ComButton} from "../../src";

describe('ComButton', () => {
    it('renders the correct classes', () => {
        const propsData: ButtonProps = {
            type: ButtonStyle.PRIMARY,
            size: ButtonSize.MEDIUM,
            delayTime: 0
        };

        const wrapper = mount(ComButton, {
            props: propsData,
            slots: {
                default: 'Click me'
            }
        });

        const button = wrapper.find('button');

        // Kiểm tra xem thẻ button có chứa class 'comButton' hay không
        expect(button.classes()).toContain('comButton');

        // Kiểm tra xem thẻ button có chứa class 'primary' hay không
        expect(button.classes()).toContain(ButtonStyle.PRIMARY);

        // Kiểm tra xem thẻ button có chứa class 'large' hay không
        expect(button.classes()).toContain(ButtonSize.MEDIUM);
    });

    it('emits click event without delay', async () => {
        const propsData: ButtonProps = {
            delayTime: 0
        };

        const wrapper = mount(ComButton, {
            props: propsData,
            slots: {
                default: 'Click me'
            }
        });

        await wrapper.find('button').trigger('click');
        expect(wrapper.emitted('click')).toBeTruthy();
    });

    it('emits click event with delay', async () => {
        vi.useFakeTimers();

        const propsData: ButtonProps = {
            delayTime: 1000
        };

        const wrapper = mount(ComButton, {
            props: propsData,
            slots: {
                default: 'Click me'
            }
        });

        await wrapper.find('button').trigger('click');
        expect(wrapper.emitted('click')).toBeFalsy();

        vi.advanceTimersByTime(1000);
        expect(wrapper.emitted('click')).toBeTruthy();

        vi.useRealTimers();
    });
});
