import { mount } from '@vue/test-utils';
import { describe, it, expect, vi } from 'vitest';
import { ImagePreviewerProps, ComImagePreviewer } from '../../src';

const propsData: ImagePreviewerProps = {
    source: undefined
};

describe('ComImagePreviewer', () => {
    let wrapper;

    beforeEach(() => {
        wrapper = mount(ComImagePreviewer, {
            props: propsData,
            slots: {
                default: '<i class="icon-image"/>'
            }
        });
    });

    it('renders the correct initial content', () => {
        const div = wrapper.find('div.comImagePreviewer');
        expect(div.exists()).toBe(true);
        expect(div.find('i.icon-image').exists()).toBe(true);
    });

    it('shows loading icon when an image is being loaded & display new image', async () => {
        const file = new Blob(['dummy content'], { type: 'image/png' });
        const mockFile = new File([file], 'test.png', { type: 'image/png' });

        // Mock FileReader
        const mockFileReader = {
            readAsDataURL: vi.fn(),
            onload: null,
            result: null,
            addEventListener: vi.fn().mockImplementation((event, callback) => {
                if (event === 'load') {
                    mockFileReader.onload = callback;
                }
            })
        };
        global.FileReader = vi.fn(() => mockFileReader);

        await wrapper.setProps({ source: mockFile });
        await wrapper.vm.$nextTick();

        expect(wrapper.find('i.icon-loading').exists()).toBe(true);

        // Simulate the file being loaded
        mockFileReader.result = 'data:image/png;base64,dummy-content';
        mockFileReader.onload();

        await wrapper.vm.$nextTick();
        expect(wrapper.find('i.icon-loading').exists()).toBe(false);
        expect(wrapper.find('div.comImagePreviewer').attributes('style')).toContain('background-image: url(data:image/png;base64,dummy-content)');
    });
});
