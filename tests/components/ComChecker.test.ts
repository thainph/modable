import { mount } from '@vue/test-utils';
import { describe, it, expect } from 'vitest';
import { CheckerProps, ComChecker } from '../../src';

const props: CheckerProps = {
    modelValue: true,
    disabled: false
};
const slots = {
    default: 'Click me!'
};
describe('ComChecker', () => {
    let wrapper;

    beforeEach(() => {
        wrapper = mount(ComChecker, {
            props,
            slots
        });
    });

    it('renders the correct classes and options', () => {
        const div = wrapper.find('div.comCheckBox');
        expect(div.exists()).toBe(true);

        const label = div.find('label');
        expect(label.exists()).toBe(true);

        const checkbox = label.find('input[type="checkbox"]');
        expect(checkbox.exists()).toBe(true);
        expect(checkbox.element.checked).toBe(props.modelValue);

        const checkMark = label.find('span.comCheckBox__checkMark');
        expect(checkMark.exists()).toBe(true);

        const labelText = label.find('span.comCheckBox__label');
        expect(labelText.exists()).toBe(true);
        expect(labelText.text()).toBe(slots.default);
    });

    it('emits update:modelValue when checkbox is changed', async () => {
        const checkbox = wrapper.find('input[type="checkbox"]');

        // Kiểm tra và thay đổi trạng thái của các checkbox
        await checkbox.setChecked(false);

        // Kiểm tra sự kiện emit
        const emittedEvents = wrapper.emitted('update:modelValue');
        expect(emittedEvents).toBeTruthy();
        expect(emittedEvents.length).toBe(1);

        // Kiểm tra giá trị emitted
        expect(emittedEvents[0][0]).toEqual(false);

        // Kiểm tra và thay đổi trạng thái của các checkbox
        await checkbox.setChecked(true);

        // Kiểm tra giá trị emitted
        expect(emittedEvents[1][0]).toEqual(true);
    });

    it('updates content when props.modelValue changes', async () => {
        await wrapper.setProps({ modelValue: true });
        expect(wrapper.find('input[type="checkbox"]').element.checked).toBe(true);

        await wrapper.setProps({ modelValue: false });
        expect(wrapper.find('input[type="checkbox"]').element.checked).toBe(false);
    });

    it('disables checkbox when props.disabled is true', async () => {
        await wrapper.setProps({ disabled: true });
        const checkbox = wrapper.find('input[type="checkbox"]');
        expect(checkbox.element.disabled).toBe(true);

        await wrapper.setProps({ disabled: false });
        expect(checkbox.element.disabled).toBe(false);
    });
});
