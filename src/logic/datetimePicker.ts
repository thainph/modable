import {DatetimePickerProps} from "../definition/props.ts";
import {DateMatrix, DateMatrixItem, DateMatrixRow, Option} from "../definition/types.ts";
import {ref} from "vue";

export const useDatetimePicker = (props: DatetimePickerProps) => {
    const getDaysOfWeek = (locale = 'default') => {
        if (locale.toString() === 'default' || locale === 'en-US' || locale === 'en') {
            return ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
        }

        if (locale.toString() === 'vi-VN' || locale.toString() === 'vi') {
            return ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'];
        }

        if (locale.toString() === 'ja-JP' || locale.toString() === 'ja') {
            return ['日', '月', '火', '水', '木', '金', '土'];
        }

        const formatter = new Intl.DateTimeFormat(locale, { weekday: 'long' });
        let daysOfWeek = [];

        for (let day = 0; day < 7; day++) {
            const date = new Date(Date.UTC(2023, 0, day + 1)); // Using a reference date with a known Sunday (2023-01-01)
            daysOfWeek.push(formatter.format(date));
        }

        return daysOfWeek;
    }
    const getMonthsOfYear = (locale = 'default') => {
        const formatter = new Intl.DateTimeFormat(locale, { month: 'long' });
        const monthsOfYear = [];

        for (let month = 0; month < 12; month++) {
            const date = new Date(Date.UTC(2023, month, 1)); // Using a reference year
            monthsOfYear.push(formatter.format(date));
        }

        return monthsOfYear;
    }
    const getYearOptions = (year: number) => {
        const options: Option[] = [];
        const startYear = props.startYear || year - 10;
        const endYear = props.endYear || year + 10;

        for (let i = startYear; i <= endYear; i++) {
            options.push({name: i.toString(), value: i});
        }

        return options;
    }
    const getMonthOptions = () => {
        const monthsOfYear = getMonthsOfYear(props.locale);
        return monthsOfYear.map((month, index) => {
            return {
                name: month,
                value: index
            }
        })
    }
    const getHourOptions = () => {
        const options: Option[] = [];
        for (let i = 0; i < 24; i++) {
            options.push({name: i.toString().padStart(2, '0'), value: i});
        }

        return options;
    }
    const getMinuteOptions = () => {
        const options: Option[] = [];
        for (let i = 0; i < 60; i++) {
            options.push({name: i.toString().padStart(2, '0'), value: i});
        }

        return options;
    }
    const convertUtcDateStringToLocalDate = (dateString: string) => {
        let utcDate = new Date();

        if (props.type === 'date') {
            const [year, month, day] = dateString.split('-').map(Number);
            utcDate = new Date(Date.UTC(year, month - 1, day, 0, 0));
        }

        if (props.type === 'time') {
            const [hour, minute] = dateString.split(':').map(Number);
            const localDate = new Date();
            utcDate = new Date(Date.UTC(localDate.getFullYear(), localDate.getMonth(), localDate.getDate(), hour, minute));
        }

        if (props.type === 'datetime') {
            const [datePart, timePart] = dateString.split(' ');
            const [year, month, day] = datePart.split('-').map(Number);
            const [hour, minute] = timePart.split(':').map(Number);
            utcDate = new Date(Date.UTC(year, month - 1, day, hour, minute));
        }

        return utcDate;
    }
    const convertDateToString = (date: Date) => {
        const year = date.getFullYear();
        const month = String(date.getMonth() + 1).padStart(2, '0');
        const day = String(date.getDate()).padStart(2, '0');
        const hours = String(date.getHours()).padStart(2, '0');
        const minutes = String(date.getMinutes()).padStart(2, '0');

        if (props.type === 'date') {
            return `${year}-${month}-${day}`;
        }

        if (props.type === 'time') {
            return `${hours}:${minutes}`;
        }

        return `${year}-${month}-${day} ${hours}:${minutes}`;
    }
    const convertLocalDateToUtcDate = (localDate: Date) => {
        return new Date(localDate.getTime() + localDate.getTimezoneOffset() * 60000);
    }
    const isSameDate = (date1: Date, date2: Date) => {
        return date1.getDate() === date2.getDate() && date1.getMonth() === date2.getMonth() && date1.getFullYear() === date2.getFullYear();
    }

    const today = new Date();
    const daysOfWeek = getDaysOfWeek(props.locale);
    const yearOptions: Option[] = getYearOptions(today.getFullYear());
    const monthOptions: Option[] = getMonthOptions();
    const hourOptions: Option[] = getHourOptions();
    const minuteOptions: Option[] = getMinuteOptions();

    const date = ref<Date>(new Date());
    const utcDate = ref<Date>(new Date());
    const matrix = ref<DateMatrix>([]);
    const year = ref<Option>()
    const month = ref<Option>()
    const hour = ref<Option>();
    const minute = ref<Option>();

    const init = (timestamp?: string) => {
        date.value = timestamp ? convertUtcDateStringToLocalDate(timestamp) : new Date()
        year.value = yearOptions.find(item => item.value === date?.value.getFullYear());
        month.value = monthOptions.find(item => item.value === date?.value.getMonth());
        hour.value = hourOptions.find(item => item.value === date?.value.getHours());
        minute.value = minuteOptions.find(item => item.value === date?.value.getMinutes());
        generateMatrix();
    }

    const generateMatrix = () => {
        if (!year.value || !month.value || !hour.value || !minute.value || !date.value) {
            return;
        }

        const firstDayOfMonth = new Date(year.value.value, month.value.value, 1);
        const lastDayOfMonth = new Date(
            month.value.value === 12 ? year.value.value + 1 : year.value.value,
            month.value.value === 12 ? 1 : month.value.value + 1,
            0);
        let dateMatrix: DateMatrix = [];

        // Build first row
        let firstRow: DateMatrixRow[] = [];

        for (let i = 0; i < firstDayOfMonth.getDay(); i++) {
            const tempDate = new Date(year.value.value, month.value.value, 1 - firstDayOfMonth.getDay() + i, hour.value.value, minute.value.value);
            const item: DateMatrixItem = {
                isDisabled: true,
                isToday: isSameDate(today, tempDate),
                isSelected: isSameDate(date.value, tempDate),
                value: convertDateToString(tempDate),
                // @ts-ignore
                date: tempDate
            }
            // @ts-ignore
            firstRow.push(item);
        }

        let day = 1;

        for (let i = firstDayOfMonth.getDay(); i < 7; i++) {
            const tempDate = new Date(year.value.value, month.value.value, day, hour.value.value, minute.value.value);
            const item: DateMatrixItem = {
                value: convertDateToString(tempDate),
                isDisabled: false,
                isToday: isSameDate(today, tempDate),
                isSelected: isSameDate(date.value, tempDate),
                // @ts-ignore
                date: tempDate
            }
            // @ts-ignore
            firstRow.push(item);
            day++;
        }

        // @ts-ignore
        dateMatrix.push(firstRow);

        // Build the rest of the matrix
        let row: DateMatrixRow = []
        // @ts-ignore
        for (let i = day; i <= lastDayOfMonth.getDate(); i++) {
            const tempDate = new Date(year.value.value, month.value.value, i, hour.value.value, minute.value.value);
            const item: DateMatrixItem = {
                value: convertDateToString(tempDate),
                isDisabled: false,
                isToday: isSameDate(today, tempDate),
                isSelected: isSameDate(date.value, tempDate),
                // @ts-ignore
                date: tempDate
            }

            row.push(item);

            if (row.length === 7) {
                dateMatrix.push(row);
                row = [];
            }

            if (i === lastDayOfMonth.getDate() && row.length > 0) {
                dateMatrix.push(row);
            }
        }

        matrix.value = dateMatrix;
    }

    const prevMonth = () => {
        if (!year.value || !month.value) {
            return;
        }
        if (month.value.value === 0) {
            month.value = monthOptions[11];
            //@ts-ignore
            year.value = yearOptions.find(item => item.value === year.value.value - 1);
        } else {
            //@ts-ignore
            month.value = monthOptions.find(item => item.value === month.value.value - 1);
        }

        generateMatrix()
    }

    const nextMonth = () => {
        if (!year.value || !month.value) {
            return;
        }

        if (month.value.value === 11) {
            month.value = monthOptions[0];
            //@ts-ignore
            year.value = yearOptions.find(item => item.value === year.value.value + 1);
        } else {
            //@ts-ignore
            month.value = monthOptions.find(item => item.value === month.value.value + 1);
        }
        generateMatrix()
    }

    const onSelectDate = (payload: DateMatrixItem) => {
        if (payload.isDisabled) {
            return
        }

        let newDate = new Date(payload.date.getTime())

        if (typeof hour.value !== 'undefined' && typeof minute.value !== 'undefined') {
            newDate.setHours(hour.value.value)
            newDate.setMinutes(minute.value.value)
        }

        date.value = newDate
        utcDate.value = convertLocalDateToUtcDate(newDate)
        generateMatrix()
    }

    const onSelectHour = (payload: number) => {
        if (!date.value) {
            return
        }

        let newDate = new Date(date.value.getTime())
        newDate.setHours(payload)
        date.value = newDate
        utcDate.value = convertLocalDateToUtcDate(newDate)

    }

    const onSelectMinute = (payload: number) => {
        if (!date.value) {
            return
        }

        let newDate = new Date(date.value.getTime())
        newDate.setMinutes(payload)
        date.value = newDate
        utcDate.value = convertLocalDateToUtcDate(newDate)
    }

    const onSelectYear = () => {
        generateMatrix()
    }

    const onSelectMonth = () => {
        generateMatrix()
    }

    return {
        daysOfWeek,
        monthOptions,
        yearOptions,
        hourOptions,
        minuteOptions,
        matrix,
        date,
        utcDate,
        year,
        month,
        hour,
        minute,
        nextMonth,
        prevMonth,
        init,
        convertDateToString,
        convertLocalDateToUtcDate,
        onSelectDate,
        onSelectHour,
        onSelectMinute,
        onSelectYear,
        onSelectMonth
    }
}
