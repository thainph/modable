import {ref} from "vue";
import {Paginator} from "../definition/types";
import {PaginatorProps} from "../definition/props";

export const usePaginator = (props: PaginatorProps) => {
    const pages = ref<number[]>([])
    const prev = ref<number>(0)
    const next = ref<number>(0)
    const calculate = (paginator: Paginator) => {
        pages.value = []
        prev.value = 0
        next.value = 0

        if (paginator.total <= 0) {
            return
        }
        if (paginator.total > 0 && paginator.total <= paginator.perPage) {
            return
        }

        let pageCount = Math.ceil(paginator.total / paginator.perPage)
        let start: number = 1
        let end: number = pageCount

        if (paginator.page > 1) {
            prev.value = paginator.page - 1
        }

        if (paginator.page < pageCount) {
            next.value = paginator.page + 1
        }

        if (pageCount > 10) {
            start = (paginator.page - 3) > 1 ? (paginator.page - 3) : 1
            end = (paginator.page + 3) < pageCount ? (paginator.page + 3) : pageCount

            if (start > 1) {
                pages.value.push(1)
            }
            if (start > 2) {
                pages.value.push(0)
            }
            for (let i = start; i <= end; i++) {
                pages.value.push(i)
            }
            if (end < pageCount - 2) {
                pages.value.push(0)
            }
            if (end < pageCount) {
                pages.value.push(pageCount)
            }
        } else {
            for (let i = start; i <= end; i++) {
                pages.value.push(i)
            }
        }
    }

    calculate({
        total: props.total,
        perPage: props.perPage,
        page: props.page
    })

    return {
        pages,
        prev,
        next,
        calculate
    }
}
