import {UploaderProps} from "../definition/props";
import {ref} from "vue";
import {randomStr} from "../ultis/helper";
import {PayloadUploadChunk, UploadFile} from "../definition/types";

export const useUploader = (props: UploaderProps) => {
    const files = ref<UploadFile[]>([])
    const fileReader = ref<any>(new FileReader())
    const isUploading = ref<boolean>(false)
    const inputDomId = ref<string>(props.domId || randomStr(10))

    const browseFile = () => {
        if (isUploading.value) {
            return
        }
        // @ts-ignore
        document.getElementById(inputDomId.value).click()
    }

    const beforeUpload = () => {
        if (typeof props.onSelect === "function") {
            const domInput = document.getElementById(inputDomId.value);
            // @ts-ignore
            return props.onSelect(domInput?.files)
        }

        return true
    }

    const onFileUploaded = (targetFileIndex: number, response: any) => {
        files.value[targetFileIndex].isUploaded = true
        files.value[targetFileIndex].uploadPercent = 100
        files.value[targetFileIndex].result = response

        if (files.value[files.value.length - 1].isUploaded) {
            isUploading.value = false
            const result = files.value.map(item => item.result)
            props.onSuccess(result)
        } else {
            fileReader.value = new FileReader();
            processFile(targetFileIndex + 1, 0, props.chunkSize)
        }
    }
    const onSelectFile = () => {
        const domInput = document.getElementById(inputDomId.value);

        // @ts-ignore
        if (domInput?.files.length > 0) {
            if (!beforeUpload()) {
                return
            }
            // @ts-ignore
            files.value = Array.from(domInput.files).map((item: File) => {
                return {
                    file: item,
                    isUploaded: false,
                    uploadPercent: 0,
                    result: null
                }
            })

            if (props.autoUpload) {
                upload();
            }
        }
    };

    const removeFile = (index: number) => {
        if (isUploading.value) {
            return
        }
        files.value.splice(index, 1)
    }
    const upload = () => {
        if (files.value.length > 0) {
            fileReader.value = new FileReader();
            isUploading.value = true
            processFile(0, 0, props.chunkSize)
        }
    }

    const processFile = (targetFileIndex: number, offset: number, range: number, hash: string = '', name: string = '') => {
        const targetFile = files.value[targetFileIndex].file

        if (offset >= targetFile.size) {
            return
        }

        if (name === '') {
            name = targetFile.name
        }

        // @ts-ignore
        fileReader.value.onloadend = async e => {
            try {
                if (e.target.readyState !== FileReader.DONE) {
                    return
                }
                let payload: PayloadUploadChunk = {
                    data: e.target.result.split('base64,')[1],
                    offset: offset / props.chunkSize,
                    eof: (offset + range) >= targetFile.size,
                    // @ts-ignore
                    name: name,
                    hash: hash,
                }

                const response:any = await props.onUpload(payload)
                files.value[targetFileIndex].uploadPercent = Math.round((offset/targetFile.size) * 100)


                if (payload.eof) {
                    onFileUploaded(targetFileIndex, response)
                } else {
                    processFile(targetFileIndex, offset + range, range, response.hash, response.name)
                }
            } catch (e) {
                isUploading.value = false
                props.onError(e)
            }
        }

        const chunk = targetFile.slice(offset, offset + range)
        // @ts-ignore
        fileReader.value.readAsDataURL(chunk)
    }

    const resetFile = () => {
        files.value = []
        isUploading.value = false

        const input: HTMLInputElement = <HTMLInputElement>document.getElementById(inputDomId.value)

        if (input) {
            input.value = ''
        }
    }

    return {
        browseFile,
        onSelectFile,
        upload,
        resetFile,
        removeFile,
        isUploading,
        inputDomId,
        files,
    }
}
