import {ref} from "vue";
import {Option, SelectOption} from "../definition/types";
import {OptionsProps} from "../definition/props";

export  const useSelectMultiple = (props: OptionsProps) => {
    const sourceOptions = ref<SelectOption[]>([])
    const selectedOptions = ref<Option[]>( [])
    const setStatus = (options: Option[]) => {
        return options.map(item => {
            const option: SelectOption = {
                name: item.name,
                value: item.value,
                is_selected: false
            }
            return option;
        })
    }
    const updateStatus = (options: SelectOption[], selected: Option | Option[]) => {
        const selectedOptions: Option[] = Array.isArray(selected) ? selected : [selected]
        const values: Array<string|number> = selectedOptions.map(item => {
            return item.value
        })

        return options.map(item => {
            item.is_selected = values.includes(item.value)
            return item
        })
    }

    const syncOptions = (options: Option[]) => {
        sourceOptions.value = setStatus(options)

        if (selectedOptions) {
            sourceOptions.value = updateStatus(sourceOptions.value, selectedOptions.value)
        }
    }

    const syncSelectedOptions = (selected: Option | Option[] | undefined) => {
        if (typeof selected !== 'undefined') {
            selectedOptions.value = Array.isArray(selected) ? selected : [selected]
            sourceOptions.value = updateStatus(sourceOptions.value, selected)
        }
    }

    syncOptions(props.options)
    syncSelectedOptions(props.modelValue)

    return {
        selectedOptions,
        sourceOptions,
        syncOptions,
        syncSelectedOptions,
    }
}
