import {Option, TableColumn} from "./types";
import {
    ButtonSize,
    ButtonStyle,
    DropdownTrigger,
    ModalPosition,
    NotificationPosition,
    NotificationStyle
} from "./enums";

export interface ButtonProps {
    delayTime?: number,
    type?: ButtonStyle,
    size?: ButtonSize
}
export interface OptionProps {
    modelValue?: Option,
    options: Option[],
}

export interface OptionsProps {
    modelValue?: Option[],
    options: Option[]
}
export interface SelectProps extends OptionProps {
    modelValue?: Option,
    options: Option[],
    prefixIcon?: string,
    placeholder?: string,
    onSearch?: Function,
    readonly ?: boolean
}

export interface SelectMultipleProps extends OptionsProps {
    prefixIcon?: string,
    placeholder?: string,
    onSearch?: Function,
    readonly ?: boolean
}

export interface CheckerProps {
    modelValue: boolean,
    disabled?: boolean
}

export interface UploaderProps {
    onUpload: Function,
    onSuccess: Function,
    onError: Function,
    onSelect?: Function,
    autoUpload?: boolean,
    showProgressBar?: boolean,
    domId?: string,
    mimetypes?: string,
    chunkSize: number,
    multiple?: boolean,
}

export interface ImagePreviewerProps {
    source?: File
}

export interface TableProps {
    rows: Array<any>,
    columns: TableColumn[],
    stripe?: boolean,
    border?: boolean,
    onSelect?: Function
}

export interface TextBoxProps {
    modelValue: string,
    placeholder?: string,
    prefixIcon?: string,
    suffixIcon?: string,
    delayTime?: number,
    inputType?: string,
    readonly?: boolean,
    maxLength?: number,
    onFocusIn?: Function,
    onFocusOut?: Function,
    onEnter?: Function,
}

export interface DatetimePickerProps {
    modelValue?: string,
    locale?: string,
    startYear?: number
    endYear?: number,
    type: 'date' | 'time' | 'datetime',
}
export interface TagProps {
    data?: any,
    onClose?: Function,
    onClick?: Function
}

export interface PaginatorProps {
    total: number,
    perPage: number,
    page: number,
    onChange: Function
}

export interface ListProps {
    list: Option[],
    onClick?: Function
}

export interface TabsProps {
    tabs: Option[],
    tab: Option,
    onChange?: Function,
    flat?: boolean
}

export interface DropdownProps {
    trigger: DropdownTrigger,
    isShowDropdown?: boolean,
    displayFixed?: boolean
}

export interface ModalProps {
    visible: boolean,
    transparent?: boolean,
    position?: ModalPosition
}

export interface ProgressBarProps {
    percent: number,
    showPercent?: boolean
}

export interface NotificationProps {
    id: string,
    type: NotificationStyle,
    title: string,
    content: string
}
export interface NotificationProviderProps {
    position?: NotificationPosition
}
