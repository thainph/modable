import {NotificationProps} from "./props";

export interface NotificationState {
    notifications: NotificationProps[]
}
