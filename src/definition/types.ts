export type Option = {
    name: string,
    value: any
}

export type SelectOption = {
    name: string,
    value: any,
    is_selected: boolean
}

export type DateMatrixItem = {
    value: string,
    isToday?: boolean,
    isSelected?: boolean,
    isDisabled?: boolean,
    date: Date
}
export type DateMatrixRow = DateMatrixItem[]
export type DateMatrix = DateMatrixRow[]
export type TableColumn = {
    name: string,
    key: string
}

export type Paginator = {
    total: number,
    perPage: number,
    page: number,
}

export type PayloadUploadChunk = {
    data: string,
    name: string,
    offset: number,
    eof: boolean,
    hash?: string
}

export type UploadFile = {
    file: File,
    isUploaded: boolean,
    uploadPercent: number,
    result?: any
}
