export enum DropdownTrigger {
    HOVER = 'hover',
    CLICK = 'click',
    STATE = 'state'
}

export enum ModalPosition {
    CENTER = 'comModal--center',
    TOP_LEFT = 'comModal--top-left',
    TOP_CENTER = 'comModal--top-center',
    TOP_RIGHT = 'comModal--top-right',
    BOT_LEFT = 'comModal--bot-left',
    BOT_CENTER = 'comModal--bot-center',
    BOT_RIGHT = 'comModal--bot-right',
}

export enum NotificationPosition {
    TOP_LEFT = 'notificationProvider--top-left',
    TOP_CENTER = 'notificationProvider--top-center',
    TOP_RIGHT = 'notificationProvider--top-right',
    BOT_LEFT = 'notificationProvider--bot-left',
    BOT_CENTER = 'notificationProvider--bot-center',
    BOT_RIGHT = 'notificationProvider--bot-right',
}

export enum ButtonStyle {
    PRIMARY = 'comButton--primary',
    LINK = 'comButton--link',
    TRANSPARENT = 'comButton--transparent',
    SUCCESS = 'comButton--success',
    WARNING = 'comButton--warning',
    DANGER = 'comButton--danger',
    CUSTOM = 'comButton--custom',
}

export enum ButtonSize {
    SMALL = 'comButton--sm',
    MEDIUM = 'comButton--md',
    LARGE = 'comButton--lg',
    XL = 'comButton--xl',
    XXL = 'comButton--2xl',
}

export enum NotificationStyle {
    PRIMARY = 'comNotification--primary',
    SUCCESS = 'comNotification--success',
    WARNING = 'comNotification--warning',
    DANGER = 'comNotification--danger',
}
