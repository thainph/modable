const mix = require('laravel-mix')

mix.setPublicPath('dist')
mix.sass('src/index.scss', 'dist/modable.css').version();

mix.sass('src/_light.scss', 'dist/light.css').version();
mix.sass('src/_light_teal.scss', 'dist/light-teal.css').version();
mix.sass('src/_light_blue.scss', 'dist/light-blue.css').version();

mix.sass('src/_dark.scss', 'dist/dark.css').version();
mix.sass('src/_dark_blue.scss', 'dist/dark-blue.css').version();
mix.sass('src/_dark_teal.scss', 'dist/dark-teal.css').version();

mix.options({
    postCss: [
        require("tailwindcss"),
    ]
});
