import {NotificationState} from "../definition/states";
import {reactive, readonly} from 'vue';
import {randomStr} from "../ultis/helper";
import {NotificationStyle} from "../definition/enums";

const state = reactive<NotificationState>({
    notifications: []
})

const actions = {
    removeNotification(id: string) {
        state.notifications = state.notifications.filter(notification => notification.id !== id)
    },
    dispatchNotification(title: string, content: string, type: NotificationStyle, autoClose?: boolean, timer?: number) {
        const id = randomStr(20)
        state.notifications = [{
            id,
            type,
            title,
            content
        }, ...state.notifications]

        if (autoClose) {
            setTimeout(() => {
                actions.removeNotification(id);
            }, timer ?? 3000);
        }
    }
}

const getters = {
    getNotifications() {
        return [...state.notifications].slice(0,4); // Get max 5 notifications
    },
    getNotificationsCount() {
        return state.notifications.length;
    }
}

export default {
    state: readonly(state),
    actions,
    getters
}
