import {Option, SelectOption} from "../definition/types";

export function isTypeOption(item: any) {
    return (item as Option).name !== undefined && (item as Option).value !== undefined
}

export function isTypeSelectOption(item: any) {
    return isTypeOption(item) && (item as SelectOption).is_selected !== undefined
}
export function randomStr(length: number = 10) {
    let result = '';
    let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;

    for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return result;
}
