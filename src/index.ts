// atoms
import ComButton from "./components/atoms/button/ComButton.vue";
import ComCheckBox from "./components/atoms/checkbox/ComCheckBox.vue";
import ComChecker from "./components/atoms/checker/ComChecker.vue";
import ComDropdown from "./components/atoms/dropdown/ComDropdown.vue";
import ComImagePreviewer from "./components/atoms/image-previewer/ComImagePreviewer.vue";
import ComList from "./components/atoms/list/ComList.vue";
import ComModal from "./components/atoms/modal/ComModal.vue";
import ComNotification from "./components/atoms/notification/ComNotification.vue";
import ComPaginator from "./components/molecules/paginator/ComPaginator.vue";
import ComProgressBar from "./components/atoms/progress-bar/ComProgressBar.vue";
import ComRadio from "./components/atoms/radio/ComRadio.vue";
import ComSelectBox from "./components/atoms/select-box/ComSelectBox.vue";
import ComSelectMultipleBox from "./components/molecules/select-multiple-box/ComSelectMultipleBox.vue";
import ComSwitcher from "./components/atoms/switcher/ComSwitcher.vue";
import ComTabs from "./components/atoms/tabs/ComTabs.vue";
import ComTag from "./components/atoms/tag/ComTag.vue";
import ComTextArea from "./components/atoms/textarea/ComTextArea.vue";
import ComTextBox from "./components/atoms/textbox/ComTextBox.vue";
import ComVideoPreviewer from "./components/atoms/video-previewer/ComVideoPreviewer.vue";

//molecules
import ComChunkUploader from "./components/molecules/chunk-uploader/ComChunkUploader.vue";
import ComTable from "./components/molecules/table/ComTable.vue";
import ComTransfer from "./components/molecules/transfer/ComTransfer.vue";
import ComDatetimePicker from "./components/molecules/datetime-picker/ComDatetimePicker.vue";

// Providers
import NotificationProvider from "./components/providers/NotificationProvider.vue";

// Directives

// Store
import NotificationStore from './stores/notification'
import {App} from "vue";

// Definition
export * from "./definition/enums";
export * from "./definition/props";
export * from "./definition/types";

// Logic
export * from "./logic/select";
export * from "./logic/paginator";
export * from "./logic/datetimePicker";
export * from "./logic/uploader";

const Modable = {
    install: (app: App) => {
        app.component('ComButton', ComButton);
        app.component('ComCheckBox', ComCheckBox);
        app.component('ComChecker', ComChecker);
        app.component('ComDropdown', ComDropdown);
        app.component('ComImagePreviewer', ComImagePreviewer);
        app.component('ComList', ComList);
        app.component('ComModal', ComModal);
        app.component('ComNotification', ComNotification);
        app.component('ComPaginator', ComPaginator);
        app.component('ComProgressBar', ComProgressBar);
        app.component('ComRadio', ComRadio);
        app.component('ComSelectBox', ComSelectBox);
        app.component('ComSelectMultipleBox', ComSelectMultipleBox);
        app.component('ComSwitcher', ComSwitcher);
        app.component('ComTabs', ComTabs);
        app.component('ComTag', ComTag);
        app.component('ComTextArea', ComTextArea);
        app.component('ComTextBox', ComTextBox);
        app.component('ComVideoPreviewer', ComVideoPreviewer);
        app.component('ComChunkUploader', ComChunkUploader);
        app.component('ComTable', ComTable);
        app.component('ComTransfer', ComTransfer);
        app.component('ComDatetimePicker', ComDatetimePicker);
        app.component('NotificationProvider', NotificationProvider);
        app.provide('NotificationState', NotificationStore);
    }
}

export {
    ComButton,
    ComCheckBox,
    ComChecker,
    ComDropdown,
    ComImagePreviewer,
    ComList,
    ComModal,
    ComNotification,
    ComPaginator,
    ComProgressBar,
    ComRadio,
    ComSelectBox,
    ComSelectMultipleBox,
    ComSwitcher,
    ComTabs,
    ComTag,
    ComTextArea,
    ComTextBox,
    ComVideoPreviewer,
    ComChunkUploader,
    ComTable,
    ComTransfer,
    ComDatetimePicker,
    NotificationProvider,
    NotificationStore
}
export default Modable;
