import {ButtonSize, ButtonStyle, ModalPosition, NotificationPosition, NotificationStyle} from "modable";

export const selectOptions = [
    {
        name: 'Option 1',
        value: 1,
    },
    {
        name: 'Option 2',
        value: 2,
    },
    {
        name: 'Option 3',
        value: 3,
    },
    {
        name: 'Option 4',
        value: 4,
    },
    {
        name: 'Option 5',
        value: 5,
    },
    {
        name: 'Option 6',
        value: 6,
    },
    {
        name: 'Option 7',
        value: 7,
    },
    {
        name: 'Option 8',
        value: 8,
    },
    {
        name: 'Option 9',
        value: 9,
    },
    {
        name: 'Option 10',
        value: 10,
    }
]

export const buttonStyles = [
    {
        name: 'Default',
        value: '',
    },
    {
        name: 'Primary',
        value: ButtonStyle.PRIMARY,
    },
    {
        name: 'Success',
        value: ButtonStyle.SUCCESS,
    },
    {
        name: 'Warning',
        value: ButtonStyle.WARNING,
    },
    {
        name: 'Danger',
        value: ButtonStyle.DANGER,
    },
    {
        name: 'Link',
        value: ButtonStyle.LINK,
    },
    {
        name: 'Transparent',
        value: ButtonStyle.TRANSPARENT,
    },
    {
        name: 'Custom',
        value: ButtonStyle.CUSTOM,
    }
];
export const buttonSizes = [
    {
        name: 'Default',
        value: '',
    },
    {
        name: 'Small',
        value: ButtonSize.SMALL,
    },
    {
        name: 'Medium',
        value: ButtonSize.MEDIUM,
    },
    {
        name: 'Large',
        value: ButtonSize.LARGE,
    },
    {
        name: 'XL',
        value: ButtonSize.XL,
    },
    {
        name: '2XL',
        value: ButtonSize.XXL,
    },
];

export const tableData = [
    {
        name: 'John Doe',
        age: 29,
        job: 'PHP developer'
    },
    {
        name: 'Bruce Wayne',
        age: 35,
        job: 'Broker'
    },
    {
        name: 'Leo Nguyen',
        age: 20,
        job: 'Singer'
    },
    {
        name: 'Jeniffer Lopes',
        age: 19,
        job: 'Student'
    },
    {
        name: 'Franky Tran',
        age: 42,
        job: 'Trader'
    }
];

export const tableColumns = [
    {
        name: 'Full name',
        key: 'name'
    },
    {
        name: 'Age',
        key: 'age'
    },
    {
        name: 'Job',
        key: 'job'
    }
];

export const tagStyles= [
    {
        name: 'Default',
        value: '',
    },
    {
        name: 'Primary',
        value: 'comTag--primary',
    },
    {
        name: 'Success',
        value: 'comTag--success',
    },
    {
        name: 'Warning',
        value: 'comTag--warning',
    },
    {
        name: 'Danger',
        value: 'comTag--danger',
    }
];

export const modalPositions = [
    {
        name: 'Center',
        value: ModalPosition.CENTER
    },
    {
        name: 'Top left',
        value: ModalPosition.TOP_LEFT
    },
    {
        name: 'Top center',
        value: ModalPosition.TOP_CENTER
    },
    {
        name: 'Top right',
        value: ModalPosition.TOP_RIGHT
    },
    {
        name: 'Bot left',
        value: ModalPosition.BOT_LEFT
    },
    {
        name: 'Bot center',
        value: ModalPosition.BOT_CENTER
    },
    {
        name: 'Bot right',
        value: ModalPosition.BOT_RIGHT
    }
];
export const notificationPositions = [
    {
        name: 'Top left',
        value: NotificationPosition.TOP_LEFT
    },
    {
        name: 'Top center',
        value: NotificationPosition.TOP_CENTER
    },
    {
        name: 'Top right',
        value: NotificationPosition.TOP_RIGHT
    },
    {
        name: 'Bot left',
        value: NotificationPosition.BOT_LEFT
    },
    {
        name: 'Bot center',
        value: NotificationPosition.BOT_CENTER
    },
    {
        name: 'Bot right',
        value: NotificationPosition.BOT_RIGHT
    }
];
export const notificationStyles = [
    {
        name: 'Primary',
        value: NotificationStyle.PRIMARY
    },
    {
        name: 'Success',
        value: NotificationStyle.SUCCESS
    },
    {
        name: 'Warning',
        value: NotificationStyle.WARNING
    },
    {
        name: 'Danger',
        value: NotificationStyle.DANGER
    }
];


