import { createRouter, createWebHashHistory } from "vue-router";
import DemoButton from "./pages/DemoButton.vue";
import DemoCheckBox from "./pages/DemoCheckBox.vue";
import DemoChecker from "./pages/DemoChecker.vue";
import DemoChunkUploader from "./pages/DemoChunkUploader.vue";
import DemoPaginator from "./pages/DemoPaginator.vue";
import DemoRadio from "./pages/DemoRadio.vue";
import DemoSelectBox from "./pages/DemoSelectBox.vue";
import DemoSelectMultipleBox from "./pages/DemoSelectMultipleBox.vue";
import DemoTable from "./pages/DemoTable.vue";
import DemoTag from "./pages/DemoTag.vue";
import DemoTextBox from "./pages/DemoTextBox.vue";
import DemoTabs from "./pages/DemoTabs.vue";
import DemoDropdown from "./pages/DemoDropdown.vue";
import DemoModal from "./pages/DemoModal.vue";
import DemoNotification from "./pages/DemoNotification.vue";
import DemoTransfer from "./pages/DemoTransfer.vue";
import DemoIcon from "./pages/DemoIcon.vue";
import DemoList from "./pages/DemoList.vue";
import DemoProgressBar from "./pages/DemoProgressBar.vue";
import DemoDatetimePicker from "./pages/DemoDatetimePicker.vue";
import Home from "./pages/Home.vue";

export const routes= {
    home: {
        path: "/",
        name: "About Modable",
        component: Home
    },
    button: {
        path: "/button",
        name: "Button",
        component: DemoButton
    },
    checkbox: {
        path: "/checkbox",
        name: "CheckBox",
        component: DemoCheckBox
    },
    checker: {
        path: "/checker",
        name: "Checker",
        component: DemoChecker
    },
    chunkUploader: {
        path: "/chunk-uploader",
        name: "ChunkUploader",
        component: DemoChunkUploader
    },
    paginator: {
        path: "/paginator",
        name: "Paginator",
        component: DemoPaginator
    },
    radio: {
        path: "/radio",
        name: "Radio",
        component: DemoRadio
    },
    selectBox: {
        path: "/select-box",
        name: "SelectBox",
        component: DemoSelectBox
    },
    selectMultipleBox: {
        path: "/select-multiple-box",
        name: "SelectMultipleBox",
        component: DemoSelectMultipleBox
    },
    table: {
        path: "/table",
        name: "Table",
        component: DemoTable
    },
    tag: {
        path: "/tag",
        name: "Tag",
        component: DemoTag
    },
    textBox: {
        path: "/text-box",
        name: "TextBox",
        component: DemoTextBox
    },
    datetimePicker: {
        path: "/datetime-picker",
        name: "DatetimePicker",
        component: DemoDatetimePicker
    },
    tabs: {
        path: "/tabs",
        name: "Tabs",
        component: DemoTabs
    },
    list: {
        path: "/list",
        name: "List",
        component: DemoList
    },
    progressBar: {
        path: "/progress-bar",
        name: "ProgressBar",
        component: DemoProgressBar
    },
    dropdown: {
        path: "/dropdown",
        name: "Dropdown",
        component: DemoDropdown
    },
    modal: {
        path: "/modal",
        name: "Modal",
        component: DemoModal
    },
    notification: {
        path: "/notification",
        name: "Notification",
        component: DemoNotification
    },
    transfer: {
        path: "/transfer",
        name: "Transfer",
        component: DemoTransfer
    },
    icon: {
        path: "/icon",
        name: "Icon",
        component: DemoIcon
    },

}

const router = createRouter({
    history: createWebHashHistory(),
    routes: Object.keys(routes).map(key => {
        // @ts-ignore
        return routes[key]
    }),
});

export default router;
