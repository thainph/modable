import { createApp } from 'vue'
import App from './App.vue'
import Modable from 'modable'
import router from './router'
import '../../src/theme/dist/modable.css'
import './css/index.css'

const app = createApp(App)
// @ts-ignore
app.use(Modable)
app.use(router)
app.mount('#app')
